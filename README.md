# web admin

## 第一步 安装依赖
```
npm install
```

## 第二步 启动项目（开发模式）
```
npm run dev
```

### 打包发布
```
npm run build
```
