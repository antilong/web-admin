
export const rendered = {
  center (h, params) {
    return h('div', { style: { textAlign: 'center' } }, params.column.title)
  }
}
