import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import './assets/icons/iconfont.css'
import './styles/app.styl'

// 打包时不引入mock
if (process.env.NODE_ENV !== 'production') require('@/mock')

Vue.use(iView)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
