export const depts = req => {
  return [
    { name: '产品研发部', employees: 10, director: '陈圆圆' },
    { name: '项目实施部', employees: 30, director: '李师师' },
    { name: '信用研究院', employees: 5, director: '苏小小' }
  ]
}
