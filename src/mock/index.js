import Mock from 'mockjs'
import { depts } from './data'

Mock.mock(/depts/, depts)

export default Mock
