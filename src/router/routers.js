import Index from '@/views/index.vue'

export default [
  {
    path: '/',
    name: 'index',
    component: Index
  },
  {
    path: '/system',
    name: 'system',
    component: Index,
    children: [{
      path: 'dept',
      name: 'dept',
      component: () => import('@/views/modules/system/dept.vue')
    }, {
      path: 'user',
      name: 'user',
      component: () => import('@/views/modules/system/user.vue')
    }]
  },
  {
    path: '/logger',
    name: 'logger',
    component: Index,
    children: [{
      path: 'session',
      name: 'session',
      component: () => import('@/views/modules/logger/session.vue')
    }, {
      path: 'oprater',
      name: 'oprater',
      component: () => import('@/views/modules/logger/oprater.vue')
    }]
  }
]
