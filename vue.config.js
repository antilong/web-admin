const path = require('path')

const resolve = dir => {
  return path.join(__dirname, dir)
}

// 项目部署基础
// 默认情况下，假设你的应用将被部署在域的根目录下,
// 例如：https://localhost/
// 默认：'/'
// 如果应用程序部署在子路径中，则需要在这指定子路径
// 例如：https://localhost/myapp/
// 需要将它改为'/myapp/'
const BASE_URL = process.env.NODE_ENV === 'production' ? '/credit-admin/' : '/'

module.exports = {
  baseUrl: BASE_URL,
  // 开启ESlint语法检查
  lintOnSave: true,
  chainWebpack: config => {
    config.resolve.alias.set('@', resolve('src'))
  },
  // 打包时不生成.map文件
  productionSourceMap: true,
  // 调用接口的基础路径，解决跨域，如果设置了代理，本地开发环境的axios的baseUrl要写为 '' ，即空字符串
  devServer: {
    proxy: 'http://localhost:8080'
  }
}
